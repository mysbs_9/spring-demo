# SpringDemo

#### 介绍
如何使用spring

#### 软件架构
该项目讲述了如何使用spring、spring JDBC、spring MVC 及其对持久层mybatis、Hibernate框架进行整合使用。

#### 项目结构说明

#### 一、spring

**1、spring的原理及其特点**

spring框架是一个全面、企业应用开发一战式的解决方案，贯穿表现层、业务层、持久层。而且spring仍然可以和其他框架无缝结合。

**特点：**

轻量级、控制反转、面向切面、容器、无缝集成其他框架

轻量级：从大小与开销方面而言Spring都是轻量级的。完整的Spring大小只有1M多的jar文件里发布，并且spring所需的处理开销也是微不足道的。

控制反转：spring通过一种称作控制反转IOC技术促进低耦合。

面向切面：Spring支持面向切面编程，并且把应用业务逻辑和系统服务分开

**容器：**spring包含并管理应用对象的配置和生命周期,在这个意义上它是一种容器，你可以可以配置你的每个bean如何被创建---基于一个可配置原型，你的bean可以创建一个单独的实例或者每次需要时生成一个新的实例---以及他们是如何关联的

#### **springday01:**

该包中包含了如何使用spring容器创建对象，依赖注入常用的三种方式、spring bean 的作用域。

**spring容器创建对象：**

1、使用无参构造器创建对象

class属性指定该类的全限定名（要求包名）

2、使用实例工厂创建对象

指定一个方法，spring容器会调用这个bean的方法来创建对象

3、使用静态工厂创建对象

指定一个静态方法，spring容器会通过这个静态方法创建对象。

**依赖注入的三种方式**：

1、setter方式：

2、构造器方式：

3、基于注解注入：

**spring bean作用域**：

单例（singleton）、原型（prototype）、request、session、和global session。

**spring bean的生命周期：**

#### **springday02**

改包中包含了使用注解的方式声明一个类为bean及其生命作用域是否延迟加载、初始方法、销毁方法和spring EL表达式的用法。

**注解：**

@Component：通用注解，可以使用声明该类是一个可自动装配的bean，bean的id可自己声明缺省值则为首字母小写之后的类名。

@Scope("")：声明作用域

@Lazy()：是否延迟加载,ApplicationContext实现的默认行为就是在启动spring容器时将所有

@PostConstruct：声明初始方法

@PreDestory：声明销毁方法

**springEL**

什么是springEL

```xml
Spring3中引入了Spring表达式语言—SpringEL,SpEL是一种强大,简洁的装配Bean的方式。
他可以通过运行期间执行的表达式将值装配到我们的属性或构造函数当中,更可以调用JDK中提供的静态常量,获取外部Properties文件中的的配置
```

为什么使用spring EL

```xml
我们平常通过配置文件或Annotaton注入的Bean,其实都可以称为静态性注入,试想一下,若然我Bean A中有变量A,它的值需要根据Bean B的B变量为参考,在这场景下静态注入就对这样的处理显得非常无力,而Spring3增加的SpringEL就可以完全满足这种需求,而且还可以对不同Bean的字段进行计算再进行赋值,功能非常强大
```

如何使用speingEL

```XML
SpringEL从名字来看就能看出,和EL是有点关系的,SpringEL的使用和EL表达式的使用非常相似,
EL表达式在JSP页面更方便的获取后台中的值,而SpringEL就是为了更方便获取Spring容器中的Bean的值,
EL使用${},而SpringEL使用#{}进行表达式的声明。
```

#### springday03

@Autowired和@Resource的区别

@Autowired：自动装配，默认根据类型注入。支持setter方式和构造器方式注入。可以将该注解添加到对应的set方法和构造器方法前面。

@Resource：默认按照名称装配。只支持setter方式注入，使用name属性指定要注入的bean的id（如果不指定会按照byType的方式注入）也可以将注解添加到属性前

#### MyBatis

day01:

**mybatis核心组件：**

1、SqlSessionFactoryBuilder（构造器）：它会根据配置或者代码来生成SqlSessionFactory，采用的是分步构建的 Builder 模式。

2、SqlSessionFactory（工厂接口）：依靠调用openSession方法来生成 SqlSession，使用的是工厂模式。

3、SqlSession（会话接口）：一个既可以发送 SQL 执行返回结果，也可以获取 Mapper 的接口（调用getMapper方法）。在现有的技术中，一般我们会让其在业务逻辑代码中“消失”，而使用的是 MyBatis 提供的 SQL Mapper 接口编程技术，它能提高代码的可读性和可维护性。

4、SQL Mapper（映射器）:MyBatis 新设计存在的组件，它由一个 [Java](http://c.biancheng.net/java/) 接口和 XML 文件（或注解）构成，需要给出对应的 SQL 和映射规则。它负责发送 SQL 去执行，并返回结果。

![mybatis核心组件](F:\IntelliJ WokeSpace\springdemo\images~~~~\mybatis核心组件.png)

dy02

**一对一关联查询：**

一对一级联关系在现实生活中是十分常见的，例如一个大学生只有一张一卡通，一张一卡通只属于一个学生。再如人与身份证的关系也是一对一的级联关系。

在 MyBatis 中，通过 <resultMap> 元素的子元素 <association> 处理这种一对一级联关系。

**一对多关联查询：**

例如一个用户可以有多个订单，而一个订单只属于一个用户。

通过 <resultMap> 元素的子元素 <collection> 处理这种一对一级联关系。

**多对多关联查询：**

MyBatis 没有实现多对多级联，这是因为多对多级联可以通过两个一对多级联进行替换。一个订单可以有多种商品，一种商品可以对应多个订单，订单与商品就是多对多的级联关系，使用一个中间表（订单记录表）就可以将多对多级联转换成两个一对多的关系。



#### Hibernate

#### spring-mybatis

#### spring-hibernate

#### spring-jdbc

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

