package springhibernate.dao;

import org.springframework.stereotype.Repository;
import springhibernate.entity.Dept;


public interface DeptDao {
    Dept findDeptById(Integer id);
    int insertDept(Dept dept);
    void updateDept(Dept dept);
    void deleteDept(Dept dept);
}
