package springhibernate.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import springhibernate.entity.Dept;

@Repository("deptDao")
public class DeptDaoHibernateTemplate implements DeptDao{
    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Override
    public Dept findDeptById(Integer id) {
        return hibernateTemplate.get(Dept.class,id);
    }

    @Override
    public int insertDept(Dept dept) {
        Object i=hibernateTemplate.save(dept);
        if(i!=null){
            return 1;//返回1表示新增数据成功
        }
        return 0;
    }

    @Override
    public void updateDept(Dept dept) {
        hibernateTemplate.update(dept);
    }

    @Override
    public void deleteDept(Dept dept) {
        hibernateTemplate.delete(dept);
    }
}
