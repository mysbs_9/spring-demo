package springmvc.day03.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SomeInterceptor implements HandlerInterceptor {
    /**
     * DisPatcherServlet(前端控制器）收到请求之后，会先调用preHandel方法
     * 如果该方法的返回值为true时，则继续向后调用，如果返回值为false则不再继续
     * 调用后面的方法
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        System.out.println("preHandle()");
        return true;
    }

    /**
     * 处理器Controller的方法已经执行完毕，正准备将处理结果（ModelAndView对象）返回
     * 给DispatcherServlet之前执行postHandle方法。可以在该方法里修改处理结果
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle()");
    }

    /**
     * 最后执行的方法
     * 注意：只有当preHandle方法值为true时，该方法才会执行
     * e:是处理器所抛出的异常，可以写一个拦截器，用来处理这些异常
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param e
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        System.out.println("afterCompletion()");
    }
}
