package springmvc.day03.exceptionHandler;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HelloController {
    @RequestMapping("/hello.do")
    public String hello(){
        System.out.println("hello()");
        //发生异常
        int i=Integer.parseInt("1000a");
        System.out.println(i);
        return "/hello";
    }
    @RequestMapping("/hello1.do")
    public String hello2(){
        System.out.println("hello2()");
        String str="abcd";
        char i=str.charAt(10);
        System.out.println(i);
        return "/hello";
    }

    /**
     * 程序发现异常返回异常界面的处理方式二
     *
     * 使用@ExceptionHandler注解
     * step1：在处理其中添加异常处理方法该方法必须使用@ExceptionHandler修饰。
     * 注：在该方法里，依据异常类型分别进行不同的处理
     * step2：添加异常处理界面
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler
    public String exHandler(Exception ex, HttpServletRequest request){
        if(ex instanceof NumberFormatException){
            request.setAttribute("errorMsg","请输入正确的数字");
            return "/error";
        }else if(ex instanceof StringIndexOutOfBoundsException){
            request.setAttribute("errorMsg","下标越界");
            return "/error";
        }else{
            return "/system_error";
        }
    }
}
