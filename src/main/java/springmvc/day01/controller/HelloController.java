package springmvc.day01.controller;



import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HelloController implements Controller {
    /**
     * ModelAndView有俩个构造器
     * 1、ModelAndView(String viewName)
     * 2、ModelAndView(String viewName,Map date)
     * Map用于封装处理结果的数据
     */
    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        System.out.println("handelRequest()");
        return new ModelAndView("/hello");
    }
    /**
     * 认识spring mvc：
     * Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面。
     * Spring 框架提供了构建 Web 应用程序的全功能 MVC 模块。使用 Spring 可插入的 MVC 架构，
     * 从而在使用Spring进行WEB开发时，可以选择使用Spring的Spring MVC框架或集成其他MVC开发框架，
     * MVC作为WEB项目开发的核心环节，正如三个单词的分解那样，C（控制器）将V（视图、用户客户端）与M（javaBean:封装数据）分开构成了MVC ，
     *
     * spring mvc的五大核心组件
     * 1、DispatcherServlet（前端控制器）
     * 接受请求，依据HandlerMapping的配置用相应的模型来处理
     * 2、HandlerMapping
     * 包含请求路径与模型的关系
     * 3、Controller
     * 负责处理业务逻辑
     * ModelAndView
     * 封装了处理结果
     * 5、ViewResolver
     * DispatcherServlet依据ViewResolver的解析，调用真正
     * 的视图对象来生成相应的页面
     */
}
