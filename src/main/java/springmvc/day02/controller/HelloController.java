package springmvc.day02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import springmvc.day02.entity.AdminParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HelloController {
    /**
     * 如何使用@Controller注解编写一个处理器
     * 1、不用实现Controller接口
     * 2、可以在处理器中添加多个方法每个方法处理一种类型的请求
     * 3、方法名不做要求，返回类型可以是ModelAndView也可以是String
     * 4、使用@Controller,将该处理其纳入spring容器管理。（也就是说spring配置文件不用配置该处理器了）
     * 5、使用@RequestMapping,告诉前端控制器（DisPatchServlet）请求路径与处理器方法的对应关系（spring配置文件不用配置HandelMapping）
     */
    @RequestMapping("/hello.do")
    public String hello(){
        System.out.println("hello()");
        return "/hello";
    }
    @RequestMapping("/toLogin.do")
    public String toLogin(){
        return "/login";
    }

    /**
     * 读取请求参数一
     * 通过HttpServletRequest对象读取请求参数值
     * 将HttpServletRequest对象作为方法的参数即可
     */
    @RequestMapping("/login.do")
    public String login(HttpServletRequest request){
        String admin=request.getParameter("admin");
        String password=request.getParameter("password");
        System.out.println("admin:"+admin+"password:"+password);
        return "/index";
    }
    /**
     * 读取请求参数二
     * 使用@RequestParam注解，将该注解添加到形参前面
     * 实际参数名和请求参数名不一致时可使用
     */
    @RequestMapping("/login1.do")
    public String login1(String admin, @RequestParam("password")String pwd){
        System.out.println("admin:"+admin+"password:"+pwd);
        return "/index";
    }
    /**
     * 读取请求参数三
     * 将请求参数封装成一个JavaBean
     */
    @RequestMapping("/login2.do")
    public String login2(AdminParam adminParam){
        String admin=adminParam.getAdmin();
        String password=adminParam.getPassword();
        System.out.println(admin+":"+password);
        return "/index";
    }


    /**
     * 向页面传参方式一
     * 将数据绑定到HttpServletRequest
     */
    @RequestMapping("/login3.do")
    public String login3(HttpServletRequest request,AdminParam adminParam){
        //获取请求参数
        String admin=adminParam.getAdmin();
        //绑定数据
        request.setAttribute("admin",admin);
        return "/index";
    }
    /**
     * 向页面传参方式二
     * 使用ModelAndView(String viewName,Map date)
     */
    @RequestMapping("/login4.do")
    public ModelAndView login4(AdminParam adminParam){
        //获取参数
        String admin=adminParam.getAdmin();
        Map<String,Object> data=new HashMap<>();
        data.put("admin",admin);
        ModelAndView modelAndView=new ModelAndView("/index",data);
        return modelAndView;
    }
    /**
     * 向页面传参方式三
     * 使用ModelMap
     */
    @RequestMapping("/login5.do")
    public String login5(AdminParam adminParam, ModelMap modelMap){
        //获取参数
        String admin=adminParam.getAdmin();
        //绑定参数
        modelMap.addAttribute("admin",admin);
        return "/index";
    }
    /**
     * 向页面传参方式四
     * 使用HttpSession
     */
    @RequestMapping("/login6.do")
    public String login6(AdminParam adminParam,HttpSession session){
        //获取参数
        String admin=adminParam.getAdmin();
        //绑定参数
        session.setAttribute("admin",admin);
        return "/index";
    }


    /**
     * 重定向
     */
    //方法返回值时String时
    @RequestMapping("/login7.do")
    public String login7(){
        return "redirect:/toIndex.do";
    }
    @RequestMapping("/toIndex.do")
    public String toIndex(){
        return "/index";
    }
    //方法返回值是ModelAndView时
    @RequestMapping("/login8.do")
    public ModelAndView login8(){
        RedirectView redirectView=new RedirectView("/toIndex.do");
        return new ModelAndView(redirectView);
    }
}
