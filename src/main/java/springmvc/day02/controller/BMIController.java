package springmvc.day02.controller;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springmvc.day02.entity.AdminParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BMIController {
    @RequestMapping("/toBMI.do")
    public String toBMI(){
        return "/bmi_from";
    }
    @RequestMapping("/bmi.do")
    public String bmi(AdminParam adminParam,HttpServletRequest request){
        //获取参数
        Double height=adminParam.getHeight();
        Double weight=adminParam.getWeight();
        System.out.println(height+":"+weight);
        String status="完美身材";
        //计算bmi
        Double bmi=weight/(height*height);
        System.out.println(bmi);
        if(bmi<19){
            status="体重过轻";
        }
        if(bmi>25){
            status="过于肥胖";
        }
        //将结果传给前端
        request.setAttribute("bmi",bmi);
        request.setAttribute("status",status);
        return "/bmi_view";
    }
}
