package springday03.autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("rest")
public class Restaurant {
    public Restaurant(){
        System.out.println("Restaurant()");
    }

    /**
     * Autowired和Qualifier
     * 1、该注解支持setter方法时注入和构造器方式注入
     * 2、当采用setter方式注入时可以将Autowired添加到set方法前面。
     * 如果不使用Qualifier则容器会使用byType的方式注入有可能出错。
     * 所以建议使用注解Qualifier注解明确指定要注入的bean的id也可以将这两个注解直接添加到属性前
     * 3、当采用构造器注入时，可以将该注解添加到对应的构造器前面即可
     *
     * Resource
     * 1、只支持set方式注入
     * 2、可以将该注解添加到属性前，使用name属性指定要注入的bean的id（如果不指定会按照byType的方式注入）
     * 也可以将注解添加到属性前
     */
    @Autowired
    private Waiter waiter;
    public void setWaiter(@Qualifier("wt") Waiter waiter) {
        this.waiter = waiter;
    }

    public void Action(){
        System.out.println("Action()");
        waiter.clear();
    }

}
