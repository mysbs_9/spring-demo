package springday03.autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("bar")
public class Bar {

    //@Autowired注解构造器方式注入
    private Waiter waiter;
    @Autowired
    public Bar(Waiter waiter){
        System.out.println("Bar()");
        this.waiter=waiter;
    }

    public void Action(){
        waiter.clear();
    }
}
