package springday03.autowired;

import org.springframework.stereotype.Component;

@Component("wt")
public class Waiter {
    public Waiter(){
        System.out.println("Waiter()");
    }

    public void clear(){
        System.out.println("服务员在打扫卫生");
    }
}
