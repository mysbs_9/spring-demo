package springday03.resource;

import org.springframework.stereotype.Component;
import springday03.autowired.Waiter;

import javax.annotation.Resource;

@Component("school")
public class School {
    public School(){
        System.out.println("School()");
    }
    private Waiter waiter;
    @Resource
    public void setWaiter(Waiter waiter){
        this.waiter=waiter;
    }

    public void Action(){
        waiter.clear();
    }
}
