package springday03.resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("mg")
public class Manager {
    public Manager(){
        System.out.println("Manager()");
    }

    @Value("#{db.pageSize}")
    private String pageSize;
    @Value("sanji")
    private String name;

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
