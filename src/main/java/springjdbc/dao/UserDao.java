package springjdbc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import springjdbc.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository("userDao")
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //向用户表中插入数据
    public int insertUser(User user){
        String sql="insert into user(user_name,pass_word) values(?,?)";
        // 定义数组来存放SQL语句中的参数
        Object[] param=new Object[]{user.getUser_name(),user.getPass_word()};
        // 执行添加操作，返回受SQL语句影响的条数
        int num=jdbcTemplate.update(sql,param);
        return  num;
    }

    //根据用户id查找用户
    public User findUserById(int id){
        String sql="select * from user" + " where user_id=?";
        // 创建一个新的BeanPropertyRowMapper对象
        RowMapper<User> rowMapper=new BeanPropertyRowMapper<>(User.class);
        //创建一个object[]数组用来保存参数
        //Object[] args=new Object[]{id};
        User user=jdbcTemplate.queryForObject(sql,rowMapper,id);
        return user;
    }

    //查询所有用户信息
    public List<User> findAll(){
        String sql="select * from user";
        RowMapper<User> rowMapper=new BeanPropertyRowMapper<>(User.class);
        List<User> userList=jdbcTemplate.query(sql,rowMapper);
        return userList;
    }

    //修改用户信息
    public int modify(User user){
        String sql="update user "+" set user_name=?,pass_word=?"+" where user_id=?";
        Object[] args=new Object[]{user.getUser_name(),user.getPass_word(),user.getUser_id()};
        int row=jdbcTemplate.update(sql,args);
        return row;
    }

    public int deleteUser(int id){
        String sql="delete from user"+" where user_id=?";
        int row=jdbcTemplate.update(sql,id);
        return row;
    }
    //告诉JdbcTemplate如何将resultSet中的一条记录转换成对应的Entity对象
    /*class UserRoMapper implements RowMapper<User>{
        //rs:要处理的结果集（resultSet），index：当前正在处理的记录的下标（从0开始）
        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user=new User();
            user.setUserId(resultSet.getInt("id"));
            user.setUserName(resultSet.getString("userName"));
            user.setPassWord(resultSet.getString("passWord"));
            return user;
        }
    }*/
}
