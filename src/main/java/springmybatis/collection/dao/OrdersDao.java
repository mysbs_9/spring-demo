package springmybatis.collection.dao;

import org.springframework.stereotype.Repository;
import springmybatis.collection.entity.Orders;

import java.util.List;

@Repository("ordersDao")
public interface OrdersDao {
    //根据用户uid查询订单信息
    List<Orders> findOrdersByUserId(Integer uid);
}
