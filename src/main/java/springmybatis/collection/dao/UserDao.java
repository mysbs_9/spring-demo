package springmybatis.collection.dao;

import org.springframework.stereotype.Repository;
import springmybatis.collection.entity.User;
import springmybatis.collection.pojo.SelectUserAndOrdersById;

import java.util.List;


@Repository("userDao")
public interface UserDao {
    //根据用户id查询用户所有信息及的所有订单信息
    //对多根据uid查询用户及其关联的订单信息：级联查询的第二种方法（嵌套结果）
    User findAllUserAndOrdersById(Integer id);
    // 一对多 根据用户id查询用户信息及其关联的订单信息：级联查询的第一种方法（嵌套查询）
    User findAllUserAndOrdersById1(Integer id);
    //一对多 根据uid查询用户及其关联的订单信息：连接查询（使用POJO存储结果）
    List<SelectUserAndOrdersById> findAllUserAndOrdersById2(Integer id);

    //查询用户所有信息及的所有订单信息
    List<User> findAllUserAndOrder();
}
