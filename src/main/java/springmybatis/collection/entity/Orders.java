package springmybatis.collection.entity;

public class Orders {
    private Integer id;
    private String ordersn;
    //private String userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrdersn() {
        return ordersn;
    }

    public void setOrdersn(String ordersn) {
        this.ordersn = ordersn;
    }

    /*public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }*/

    @Override
    public String toString() {
        return "Ordersn{" +
                "id=" + id +
                ", ordersn='" + ordersn + '\'' +
                /*", userId='" + userId + '\'' +*/
                '}';
    }
}
