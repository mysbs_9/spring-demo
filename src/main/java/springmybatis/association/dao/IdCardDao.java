package springmybatis.association.dao;

import org.springframework.stereotype.Repository;
import springmybatis.association.entity.IdCard;

@Repository("idCardDao")
public interface IdCardDao {
    //根据id查询身份证号码
    IdCard findCodeById(Integer id);
}
