package springmybatis.association.dao;

import org.springframework.stereotype.Repository;
import springmybatis.association.entity.Person;
import springmybatis.association.pojo.SelectPersonAndCardById;
import springmybatis.collection.pojo.SelectUserAndOrdersById;

import java.util.List;

@Repository("personDao")
public interface PersonDao {
    //查询所有人员的信息及其身份证号码信息
    List<Person> findAllPerson();
    //一对一根据人员id查询个人信息及其身份证号码：级联查询的第二种方法（嵌套结果，执行一个SQL语句）
    Person findPersonById(Integer id);
    //一对一根据人员id查询个人信息及其身份证号码：级联查询的第一种方法（嵌套查询，执行两个SQL语句）
    Person findPersonById1(Integer id);
    //一对一根据人员id查询个人信息：连接查询（使用POJO存储结果）
    SelectPersonAndCardById findPersonById2(Integer id);

}
