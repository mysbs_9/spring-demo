package springmybatis.day02.dao;

import org.springframework.stereotype.Repository;
import springmybatis.day02.entity.Dept;

import java.util.List;

@Repository("deptDao")
public interface DeptDao {
    List<Dept> findDept();
    Dept findDeptById(Integer id);
    int insertDept(Dept dept);
    int updateDept(Dept dept);
    int deleteDept(Integer id);
}
