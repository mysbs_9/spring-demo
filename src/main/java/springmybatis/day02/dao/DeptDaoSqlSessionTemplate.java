package springmybatis.day02.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import springmybatis.day02.entity.Dept;

import javax.annotation.Resource;
import java.util.List;

//@Repository("deptDao")
public class DeptDaoSqlSessionTemplate implements DeptDao{
    @Resource(name = "sst")
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    public List<Dept> findDept() {
        return sqlSessionTemplate.selectList("springmybatis.day02.dao.DeptDao.findDept");
    }

    @Override
    public Dept findDeptById(Integer id) {
        return sqlSessionTemplate.selectOne("springmybatis.day02.dao.DeptDao.findDeptById",id);
    }

    @Override
    public int insertDept(Dept dept) {
        return sqlSessionTemplate.insert("springmybatis.day02.dao.DeptDao.insertDept",dept);
    }

    @Override
    public int updateDept(Dept dept) {
        return sqlSessionTemplate.update("springmybatis.day02.dao.DeptDao.updateDept",dept);
    }

    @Override
    public int deleteDept(Integer id) {
        return sqlSessionTemplate.delete("springmybatis.day02.dao.DeptDao.deleteDept",id);
    }
}
