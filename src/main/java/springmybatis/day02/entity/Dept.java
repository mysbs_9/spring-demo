package springmybatis.day02.entity;

import java.io.Serializable;

public class Dept implements Serializable {
    private Integer id;
    private String departmentName;
    private String loc;

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", departmentName='" + departmentName + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }
}
