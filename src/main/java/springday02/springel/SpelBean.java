package springday02.springel;

public class SpelBean {
    //如何使用springEl表达式
    private String name;
    private String city;
    private Double score;
    private String pageSize;

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public Double getScore() {
        return score;
    }

    public String getPageSize() {
        return pageSize;
    }
}
