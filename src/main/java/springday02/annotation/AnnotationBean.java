package springday02.annotation;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 通用注解 ，可标注任意类为spring组件。
 * 如果一个不知道属于哪一个层可以使用声明该类是一个可自动装配的bean，bean的id可自己声明。缺省值则为首字母小写之后的类名
 */
@Component("ab")
@Scope("singleton") //作用域为单例
/**
 * 是否延迟加载
 * ApplicationContext实现的默认行为就是在启动spring容器时将所有的singleton bean
 * 提前进行实例化（也就是依赖注入）
 */
@Lazy(true)
public class AnnotationBean {
    public AnnotationBean(){
        System.out.println("Annotation()");
    }
    /**
     * 使用注解声明一个类为bean和声明作用域、是否延迟加载、
     * 及其bean的初始方法和销毁方法
     */
    @PostConstruct
    public void init(){
        System.out.println("init()");
    }
    @PreDestroy
    public void destroy(){
        System.out.println("destroy");
    }

}
