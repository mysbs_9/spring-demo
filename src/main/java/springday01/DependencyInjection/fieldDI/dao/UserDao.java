package springday01.DependencyInjection.fieldDI.dao;

import springday01.DependencyInjection.fieldDI.entity.User;

public interface UserDao {
    int insertUser(User user);
}
