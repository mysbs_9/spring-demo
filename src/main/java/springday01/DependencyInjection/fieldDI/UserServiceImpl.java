package springday01.DependencyInjection.fieldDI;

import springday01.DependencyInjection.fieldDI.dao.UserDao;
import springday01.DependencyInjection.fieldDI.entity.User;
import org.springframework.beans.factory.annotation.Autowired;


public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;

    @Override
    public void saveUser(String username, String password) {
        User user=new User();
        user.setUsername(username);
        user.setPassword(password);
        int n=userDao.insertUser(user);
        if(n>0){
            System.out.println("注册成功");
        }
        System.out.println("注册失败");
    }
}
