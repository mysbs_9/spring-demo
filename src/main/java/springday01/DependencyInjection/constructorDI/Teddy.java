package springday01.DependencyInjection.constructorDI;

/**
 * 依赖注入的三种方式
 */

//构造器注入
public class Teddy {
    private Dog dog;
    public Teddy (Dog dog){
        this.dog=dog;
    }

    public void Action(){
        dog.yelp();
    }
}
