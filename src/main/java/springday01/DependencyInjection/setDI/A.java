package springday01.DependencyInjection.setDI;

public class A {
    public A(){
        System.out.println("A()");
    }

    private IB b;

    //在A类了调用B类里的f1方法
    public void setB(IB b) {
        System.out.println("setB()");
        this.b = b;
    }
    public void Action(){
        b.f1();
        System.out.println("Action");
    }
}
