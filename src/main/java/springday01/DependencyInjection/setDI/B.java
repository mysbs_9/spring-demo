package springday01.DependencyInjection.setDI;

public class B implements IB{
    public B(){
        System.out.println("B()");
    }
    @Override
    public void f1() {
        System.out.println("B'f1()");
    }
}
