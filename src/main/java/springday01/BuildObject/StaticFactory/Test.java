package springday01.BuildObject.StaticFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Calendar;

public class Test {
    public static void main(String[] args) {
        //使用静态工厂方法创建对象
        //通过调用类得静态方法来创建对象
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ApplicationContext.xml");
        Calendar calendar=applicationContext.getBean("calendar",Calendar.class);
        System.out.println(calendar);
    }
}
