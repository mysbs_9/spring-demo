package springday01.BuildObject.ReferencelessConstructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FirstSpring {
    /**
     * spring
     * 创建对象以及建立对象之间的依赖关系，降低对象之间的耦合度方便代码的维护。
     * spring容器
     * spring框架中的一个核心模块，用于管理对象。
     * 1、启动spring容器
     * 2、使用spring容器创建对象
     * （1）使用无参数构造器创建
     * （2）使用静态工厂方法创建
     * （3）使用实例化对象工厂方法创建
     * @param args
     */
    public static void main(String[] args) {
        /**
         * 使用spring容器创建对象
         */
        //启动spring容器
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ApplicationContext.xml");
        System.out.println(applicationContext);
        //获得student对象，之后就可以调用student类里的方法
        Student student=applicationContext.getBean("student",Student.class);
        System.out.println(student);
    }
}
