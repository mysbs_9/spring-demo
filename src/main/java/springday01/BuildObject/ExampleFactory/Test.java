package springday01.BuildObject.ExampleFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class Test {
    public static void main(String[] args) {
        //使用实例工厂方法创建对象
        //通过调用对象的实例方法来创建对象
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ApplicationContext.xml");
        //java.util.Calendar下的getTime 返回的是Date类型
        Date calendar=applicationContext.getBean("time", Date.class);
        System.out.println(calendar);
    }
}
