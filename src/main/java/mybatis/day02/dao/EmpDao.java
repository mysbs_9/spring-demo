package mybatis.day02.dao;

import mybatis.day02.entity.Emp;

import java.util.List;
import java.util.Map;

public interface EmpDao {
    List<Emp> findAll();
    Emp findEmpById(Integer id);
    Map findById(Integer id);
    int insertEmp(Emp emp);
    int updateEmp(Emp emp);
    int deleteEmp(Integer id);
}
