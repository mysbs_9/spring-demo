package hibernate.dao;

import mybatis.day02.entity.Emp;

import java.util.List;

public interface EmpDao {
    List<Emp> findAllEmp();
    Emp findEmpById();
    int insertEmp(Emp emp);
    int updateEmp(Emp emp);
    int deleteEmp(Integer id);
}
