package springday03;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springday03.autowired.Bar;
import springday03.autowired.Restaurant;
import springday03.resource.Manager;
import springday03.resource.School;

public class TestCase {

    //测试@Autowired注解setter方式注入
    @Test
    public void test(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day03/annotation.xml");
        Restaurant restaurant=applicationContext.getBean("rest", Restaurant.class);
        restaurant.Action();
    }

    //测试@Autowired注解构造器方式注入
    @Test
    public void test1(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day03/annotation.xml");
        Bar bar=applicationContext.getBean("bar", Bar.class);
        bar.Action();
    }

    //测试使用@Resource注解setter方式注入
    @Test
    public void test2(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day03/annotation.xml");
        School school=applicationContext.getBean("school", School.class);
        school.Action();
    }

    //测试@Value注解
    @Test
    public void test3(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day03/annotation.xml");
        Manager manager=applicationContext.getBean("mg", Manager.class);
        System.out.println(manager.getName());
        System.out.println(manager.getPageSize());
    }

}
