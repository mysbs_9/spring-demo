package springday01;


import springday01.DependencyInjection.constructorDI.Teddy;
import springday01.DependencyInjection.fieldDI.entity.User;
import springday01.DependencyInjection.setDI.A;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springday01.scope.MessageBean;
import springday01.scope.ScopeBean;

public class TestCase {
    @Test
    //测试作用域
    public void test1(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("scope.xml");
        //获得对象
        //作用域为多例即创建多个对象
        ScopeBean scope=applicationContext.getBean("scope", ScopeBean.class);
        ScopeBean scope1=applicationContext.getBean("scope",ScopeBean.class);
        System.out.println(scope==scope1);
    }

    @Test
    //测试生命周期
    public void test2(){
        /**
         * ApplicationContext :接口
         * AbstractApplicationContext：子接口
         * ClassPathXmlApplicationContext：实现上述接口的具体实现类
         */
        AbstractApplicationContext applicationContext=new ClassPathXmlApplicationContext("scope.xml");
        MessageBean messageBean=applicationContext.getBean("message", MessageBean.class);
        messageBean.sendMessage();
        applicationContext.close();
    }

    @Test
    //测试set方式的注入
    public void test3(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ioc.xml");
        A a=applicationContext.getBean("a1",A.class);
        a.Action();
    }

    //测试使用构造函数的方式注入
    @Test
    public void test4(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ioc.xml");
        Teddy teddy=applicationContext.getBean("teddy", Teddy.class);
        teddy.Action();
    }

    //测试通过构造器函数注入参数是否成功
    @Test
    public void test5(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ioc.xml");
        User user=applicationContext.getBean("user",User.class);
        System.out.println(user.getUserID());
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
    }
}
