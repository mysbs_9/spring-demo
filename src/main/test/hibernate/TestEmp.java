package hibernate;

import hibernate.entity.Emp;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Before;
import org.junit.Test;

public class TestEmp {
    private SessionFactory sessionFactory;
    @Before
    public void init(){
        //读取hibernate核心配置文件
        Configuration configuration=new Configuration();
        configuration.configure("hibernate/hibernateconfig.xml");
        //创建sessionFactory
        sessionFactory=configuration.buildSessionFactory();
    }

    @Test
    public void testFactory(){
        //可以创建session对象，底层是JDBC Connection，如果没有异常就说明连接数据库成功
        //使用Hibernate内置连接池(不用于生产环境!)
        Session session = sessionFactory.openSession();
        System.out.println(session);
        session.close();
    }
    @Test
    public void findEmpById(){
        //连接数据库
        Session session=sessionFactory.openSession();
        //开启事务
        //Transaction transaction = session.beginTransaction();
        //根据id查询员工
        //Object emp=session.get(Emp.class,3);
        Emp emp =session.get(Emp.class, 3);
        System.out.println(emp);
        session.close();
    }

    @Test
    public void insertEmp(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Emp emp=new Emp();
        //ids for this class must be manually assigned before calling save():这个类的id必须在调用save()之前手动分配
        emp.setId(5);
        emp.setLastName("艾斯");
        emp.setEmail("aisemail.com");
        emp.setGender(1);
        emp.setdId(2);
        session.save(emp);
        transaction.commit();
        session.close();
    }

    @Test
    public void updateEmp(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        //Object emp = session.get(Emp.class, 1);//4.1.8
        Emp emp = session.get(Emp.class, 1);
        emp.setLastName("索隆");
        session.update(emp);
        transaction.commit();
        session.close();
    }

    @Test
    public void deleteEmp(){
        Session session =null;
        Transaction transaction = null;
        try {
            session=sessionFactory.openSession();
            transaction=session.beginTransaction();
            //删除数据
            //找到要删除的数据
            //Object emp=session.get(Emp.class, "3");
            Emp emp=session.get(Emp.class, "100");
            session.delete(emp);
            transaction.commit();
        }catch(Exception e) {
            e.printStackTrace();
            if(transaction!=null) {
                transaction.rollback();
            }
        }finally {
            if(session!=null) {
                session.close();
            }
        }
    }
}
