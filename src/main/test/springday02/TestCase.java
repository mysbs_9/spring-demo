package springday02;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springday02.annotation.AnnotationBean;
import springday02.springel.CollectionBean;
import springday02.springel.SpelBean;
import springday02.springel.ValueBean;

import java.util.List;

public class TestCase {
    //测试使用setter方式向SpelBean类的属性注入值是否成功
    @Test
    public void test(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/spel.xml");
        ValueBean valueBean=applicationContext.getBean("valueBean", ValueBean.class);
        System.out.println(valueBean);
        System.out.println(valueBean.getName());
        System.out.println(valueBean.getAge());
    }

    //测试springEL表达式是否能读取参数
    @Test
    public void test2(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/spel.xml");
        SpelBean spelBean=applicationContext.getBean("spel", SpelBean.class);
        System.out.println(spelBean.getName());
        System.out.println(spelBean.getPageSize());
    }

    //测试将集合装配成bean的方式向CollectionBean注入值是否成功
    @Test
    public void test3(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/spel.xml");
        CollectionBean collectionBean=applicationContext.getBean("collectionBean", CollectionBean.class);
        List<String> collection=collectionBean.getCity();
        //遍历输出
        for(String str:collection){
            System.out.println(str);
        }
        System.out.println(collectionBean.getCity());
        System.out.println(collectionBean.getScore());
    }

    //测试注解是否生效
    @Test
    public void test4(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/annotation.xml");
        AnnotationBean annotationBean=applicationContext.getBean("ab", AnnotationBean.class);
        System.out.println(annotationBean);
    }

    //测试bean的生命周期
    @Test
    public void test5(){
        AbstractApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/annotation.xml");
        AnnotationBean annotationBean=applicationContext.getBean("ab",AnnotationBean.class);
        applicationContext.close();
    }

    //测试作用域
    @Test
    public void test6(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/annotation.xml");
        AnnotationBean annotationBean=applicationContext.getBean("ab",AnnotationBean.class);
        AnnotationBean annotationBean1=applicationContext.getBean("ab",AnnotationBean.class);
        System.out.println(annotationBean==annotationBean1);
    }

    //测试延迟加载
    @Test
    public void test7(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("day02/annotation.xml");
    }
}
