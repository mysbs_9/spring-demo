package springmybatis.day01;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.day01.dao.EmpDao;
import springmybatis.day01.entity.Emp;

import java.util.List;
import java.util.Map;

public class TestEmp {
    private EmpDao empDao;
    //实例化spring上下文，加载spring配置文件
    @Before
    public void init(){
        String path= "springmybatis/day01/spring.xml";
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext(path);
        //getBean(beanId,T.class);
        empDao= applicationContext.getBean(EmpDao.class);
    }

    //测试查询所有员工的员工信息和所在部门的信息
    @Test
    public void findAll(){
        List<Emp> all = empDao.findAll();
        for (Emp emps:all){
            System.out.println(emps);
        }
    }

    //测试根据id查询所有员工的员工信息和所在部门的信息
    @Test
    public void findEmpById(){
        Map empMap = empDao.findById(2);
        System.out.println(empMap);
    }

    //测试修改员工数据
    @Test
    public  void updateEmp(){
        Emp emp = empDao.findEmpById(1);
        System.out.println(emp);
        emp.setLastName("vinsmoke_sanji");
        emp.setGender(1);
        int i = empDao.updateEmp(emp);
        System.out.println(1);
    }

    //测试添加员工
    @Test
    public void insertEmp(){
        Emp emp=new Emp();
        emp.setLastName("萨博");
        emp.setEmail("saboemail.com");
        emp.setGender(1);
        emp.setdId(1);
        int i = empDao.insertEmp(emp);
        System.out.println(i);
    }

    //测试删除员工
    @Test
    public void deleteEmp(){
        int i = empDao.deleteEmp(1);
        System.out.println(i);
    }
}
