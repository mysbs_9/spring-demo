package springmybatis.collection;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.collection.dao.UserDao;
import springmybatis.collection.entity.Orders;
import springmybatis.collection.entity.User;
import springmybatis.collection.pojo.SelectUserAndOrdersById;

import java.util.List;

public class TestUser {
    private UserDao userDao;
    @Before
    public void init(){
        ApplicationContext applicationContext= new ClassPathXmlApplicationContext("springmybatis/collection/spring.xml");
        userDao = applicationContext.getBean("userDao", UserDao.class);
    }

    @Test
    public void findUserAndOrdersById(){
        User allUserAndOrdersById = userDao.findAllUserAndOrdersById(1);
        System.out.println(allUserAndOrdersById);
        System.out.println("-----------------------------");
        //获取用户所有订单信息
        List<Orders> ordersList = allUserAndOrdersById.getOrdersList();
        System.out.println(ordersList);

    }

    @Test
    public void findUserAndOrdersById1(){
        User allUserAndOrdersById1 = userDao.findAllUserAndOrdersById1(2);
        System.out.println(allUserAndOrdersById1);
    }

    @Test
    public void findAllUserAndOrdersById2(){
        List<SelectUserAndOrdersById> allUserAndOrdersById2 = userDao.findAllUserAndOrdersById2(1);
        for (SelectUserAndOrdersById userAndOrdersById:allUserAndOrdersById2){
            System.out.println(userAndOrdersById);
        }
    }
}
