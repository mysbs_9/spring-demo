package springmybatis.collection;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.collection.dao.OrdersDao;
import springmybatis.collection.entity.Orders;

import java.util.List;

public class TestOrders {
    private OrdersDao orderDao;
    @Before
    public void init(){
        ApplicationContext applicationContext= new ClassPathXmlApplicationContext("springmybatis/collection/spring.xml");
        orderDao = applicationContext.getBean("ordersDao", OrdersDao.class);
    }

    @Test
    public void findOrdersByUserId(){
        List<Orders> ordersByUserId = orderDao.findOrdersByUserId(3);
        for (Orders order:ordersByUserId){
            System.out.println(order);
        }
    }
}
