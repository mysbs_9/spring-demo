package springmybatis.day02;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.day02.dao.DeptDao;
import springmybatis.day02.entity.Dept;

import java.util.List;

//测试使用SqlSessionTemplate模板对数据库中的数据进行数据操作
public class TestDept {
    private DeptDao deptDao;
    @Before
    public void init(){
        String path= "springmybatis/day02/spring.xml";
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext(path);
        deptDao = applicationContext.getBean("deptDao", DeptDao.class);
    }

    @Test
    public void findDept(){
        List<Dept> dept = deptDao.findDept();
        for(Dept depts:dept){
            System.out.println(depts);
        }
    }

    @Test
    public void findDeptById(){
        Dept dept = deptDao.findDeptById(1);
        System.out.println(dept);
    }

    @Test
    public void insertDept(){
        Dept dept=new Dept();
        dept.setDepartmentName("铜锣湾扛把子");
        dept.setLoc("铜锣湾");
        int i = deptDao.insertDept(dept);
        System.out.println(i);
    }
    @Test
    public void updateDept(){
        Dept deptById = deptDao.findDeptById(3);
        deptById.setLoc("新世界");
        int i = deptDao.updateDept(deptById);
        System.out.println(i);
    }

    @Test
    public void deleteDept(){
        int i = deptDao.deleteDept(1);
        System.out.println(i);
    }
}
