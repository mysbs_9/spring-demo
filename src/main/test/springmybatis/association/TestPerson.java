package springmybatis.association;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.association.dao.PersonDao;
import springmybatis.association.entity.Person;
import springmybatis.association.pojo.SelectPersonAndCardById;


import java.util.List;

public class TestPerson {
    private PersonDao personDao;
    @Before
    public void init(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("springmybatis/association/spring.xml");
        personDao=applicationContext.getBean("personDao", PersonDao.class);
    }

    @Test
    public void findAllPerson(){
        List<Person> allPerson =personDao.findAllPerson();
        for(Person person:allPerson){
            System.out.println(person);
        }
    }

    @Test
    public void findPersonById(){
        Person personById = personDao.findPersonById(4);
        System.out.println(personById.getIdCard().getCode());
        System.out.println(personById);
    }

    @Test
    public void findPersonById1(){
        Person personById1 = personDao.findPersonById1(2);
        System.out.println(personById1);
    }

    @Test
    public void findPersonById2(){
        SelectPersonAndCardById person = personDao.findPersonById2(3);
        System.out.println(person);
    }
}
