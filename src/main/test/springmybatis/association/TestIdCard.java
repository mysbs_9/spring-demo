package springmybatis.association;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springmybatis.association.dao.IdCardDao;
import springmybatis.association.entity.IdCard;

public class TestIdCard {
    private IdCardDao idCardDao;
    @Before
    public void init(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("springmybatis/association/spring.xml");
        idCardDao=applicationContext.getBean("idCardDao",IdCardDao.class);
    }

    @Test
    public void findCodeById(){
        IdCard codeById = idCardDao.findCodeById(3);
        System.out.println(codeById);
    }
}
