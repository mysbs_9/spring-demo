package springjdbc;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import springjdbc.dao.UserDao;
import springjdbc.entity.User;


import java.sql.SQLException;
import java.util.List;

public class TestCase {
    //测试是否能连接数据库
    @Test
    public void test() throws SQLException {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("springjdbc/spring.xml");
        BasicDataSource dataSource=applicationContext.getBean("dataSource",BasicDataSource.class);
        System.out.println(dataSource.getConnection());
    }

    //将UserDao和ApplicationContext进行封装，启动测试类时提前加载init方法
    private UserDao userDao;
    @Before
    public void init(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("springjdbc/spring.xml");
        userDao=applicationContext.getBean("userDao",UserDao.class);
    }

    //测试插入用户数据是否成功
    @Test
    public void insert(){
        User user=new User();
        user.setUser_name("sanji");
        user.setPass_word("123456");
        userDao.insertUser(user);
    }

    //测试根据用户id查找用户数据
    @Test
    public void findUserById(){
        User userList=userDao.findUserById(13);
        System.out.println(userList);
    }

    //测试查询所有用户
    @Test
    public void findAll(){
        List<User> userList=userDao.findAll();
        //遍历输出
        for (User user:userList){
            System.out.println(user);
        }
    }

    //测试根据查询到的用户id更新用户数据
    @Test
    public void modify(){
        User user=userDao.findUserById(8);
        user.setUser_name("萨博");
        user.setPass_word("123456");
        int row=userDao.modify(user);
        System.out.println(row);
    }

    //测试根据id删除用户
    @Test
    public void delete(){
        int row=userDao.deleteUser(11);
        System.out.println(row);
    }
}
