package mybatis.day01;

import com.sun.org.apache.xml.internal.utils.res.XResourceBundle;
import mybatis.day01.entity.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.serviceloader.ServiceFactoryBean;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class TestEmp {
    private SqlSession sqlSession;
    @Before
    public void init() throws IOException {
        //获取核心配置文件所在位置
        String source= "mybatis/SqlMapConfig.xml";
        InputStream inputStream=Resources.getResourceAsStream(source);
        //创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
        //获得SqlSession
        sqlSession=sqlSessionFactory.openSession();
    }

    //查询所有员工的员工信息和所在部门的信息
    @Test
    public void findAllEmp(){
        List<Emp> empList=sqlSession.selectList("test.findAllEmp");
        for(Emp emp:empList){
            System.out.println(emp);
        }
        sqlSession.close();
    }

    //根据id查找员工信息及其所在在的部门信息
    @Test
    public void findAllEmpById(){
        Emp emp=sqlSession.selectOne("test.findAllEmpById",2);
        System.out.println(emp);
        sqlSession.close();
    }

    @Test
    public void findEmpById(){
        Map emp=sqlSession.selectOne("test.findEmpById",2);
        emp.get("lastName");
        System.out.println(emp);
        sqlSession.close();
    }

}
