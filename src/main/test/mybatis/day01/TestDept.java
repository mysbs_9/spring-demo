package mybatis.day01;


import mybatis.day01.entity.Dept;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class TestDept {
    /**
     *  1、获得 SqlSessionFactory
     *  2、获得 SqlSession
     *  3、调用在 mapper 文件中配置的 SQL 语句
     */
    private SqlSession sqlSession;
    @Before
    public void init() throws IOException {
        //定位核心配置文件位置
        String resource= "mybatis/SqlMapConfig.xml";
        InputStream inputStream= Resources.getResourceAsStream(resource);
        //创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
        //获取到SqlSession
        sqlSession=sqlSessionFactory.openSession();
    }

    @Test
    public void findAll(){
        // 调用 mapper 中的方法：命名空间 + id
        List<Dept> deptList=sqlSession.selectList("test.findAll");
        System.out.println(deptList);
        for(Dept list:deptList){
            System.out.println(list);
        }
        sqlSession.close();
    }

    @Test
    public void insertDept(){
        Dept dept=new Dept();
        dept.setDepartmentName("市场营销");
        dept.setLoc("杭州");
        int n=sqlSession.insert("test.insertDept",dept);
        System.out.println(n);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void findDeptById(){
        Dept dept=sqlSession.selectOne("test.findDeptById",2);
        System.out.println(dept);
        sqlSession.close();
    }

    @Test
    public void updateDept(){
        Dept dept=sqlSession.selectOne("test.findDeptById",3);
        System.out.println(dept);
        dept.setDepartmentName("电话销售");
        dept.setLoc("旧金山");
        System.out.println(dept);
        int n=sqlSession.update("test.updateDept",dept);
        System.out.println(n);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteDept(){
        int n=sqlSession.delete("test.deleteDept",3);
        System.out.println(n);
        sqlSession.commit();
        sqlSession.close();
    }
}
