package mybatis.day02;

import mybatis.day02.dao.EmpDao;
import mybatis.day02.entity.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class TestEmp {
    private SqlSession sqlSession;
    @Before
    public void init() throws IOException {
        String source="mybatis/SqlMapConfig.xml";
        //读取核心配置文件
        InputStream inputStream= Resources.getResourceAsStream(source);
        //创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
        //获得SqlSession
        sqlSession=sqlSessionFactory.openSession();
    }

    @Test
    public void findAll(){
        //getMapper方法返回一个符合Mapper映射器（EmpDao）要求的对象
        EmpDao mapper = sqlSession.getMapper(EmpDao.class);
        List<Emp> empList=mapper.findAll();
        for (Emp emp:empList){
            System.out.println(emp);
        }
        sqlSession.close();
    }

    @Test
    public void findEmpById(){
        EmpDao mapper = sqlSession.getMapper(EmpDao.class);
        Emp empById = mapper.findEmpById(2);
        System.out.println(empById);
        sqlSession.close();
    }

    @Test
    public void findById(){
        EmpDao mapper = sqlSession.getMapper(EmpDao.class);
        Map byId = mapper.findById(2);
        System.out.println(byId);
    }

}
