package springhibernate;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import springhibernate.dao.DeptDao;
import springhibernate.entity.Dept;


public class TestDept {
    private DeptDao deptDao;
    private ApplicationContext applicationContext;
    @Before
    public void init(){
        applicationContext=new ClassPathXmlApplicationContext("springhibernate/spring.xml");
        deptDao=applicationContext.getBean("deptDao",DeptDao.class);
    }

    @Test
    public void testHibernateTemplate(){
        //通过反射原理拿到HibernateTemplate
        //相当于Session对象HibernateTemplate使用更加简便
        //HibernateTemplate由spring提供的是对Session的封装，一个更简便的api
        HibernateTemplate hibernateTemplate = applicationContext.getBean("hibernateTemplate", HibernateTemplate.class);
        Dept dept=new Dept();
        dept.setId(1);
        dept.setDepartmentName("软件开发");
        dept.setLoc("深圳");
        hibernateTemplate.save(dept);
    }

    @Test
    public void findDeptById(){
        Dept deptById = deptDao.findDeptById(2);
        System.out.println(deptById);
    }

    @Test
    public void insertDept(){
        Dept dept=new Dept();
        dept.setId(1);
        dept.setDepartmentName("软件开发");
        dept.setLoc("深圳");
        deptDao.insertDept(dept);
    }

    @Test
    public void updateDept(){
        Dept deptById = deptDao.findDeptById(3);
        System.out.println(deptById);
        deptById.setLoc("厦门");
        deptDao.updateDept(deptById);
    }

    @Test
    public void deleteDept(){
        Dept dept=deptDao.findDeptById(2);
        deptDao.deleteDept(dept);
    }

}
